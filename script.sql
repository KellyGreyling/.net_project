USE [master]
GO
/****** Object:  Database [CarDealerProject]    Script Date: 2018-11-13 8:40:53 PM ******/
CREATE DATABASE [CarDealerProject]
GO
ALTER DATABASE [CarDealerProject] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CarDealerProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CarDealerProject] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CarDealerProject] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CarDealerProject] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CarDealerProject] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CarDealerProject] SET ARITHABORT OFF 
GO
ALTER DATABASE [CarDealerProject] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CarDealerProject] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CarDealerProject] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CarDealerProject] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CarDealerProject] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CarDealerProject] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CarDealerProject] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CarDealerProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CarDealerProject] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CarDealerProject] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CarDealerProject] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [CarDealerProject] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CarDealerProject] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [CarDealerProject] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CarDealerProject] SET  MULTI_USER 
GO
ALTER DATABASE [CarDealerProject] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CarDealerProject] SET ENCRYPTION ON
GO
ALTER DATABASE [CarDealerProject] SET QUERY_STORE = ON
GO
ALTER DATABASE [CarDealerProject] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 7), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 10, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [CarDealerProject]
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_ADAPTIVE_JOINS = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_MEMORY_GRANT_FEEDBACK = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_ON_ROWSTORE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET DEFERRED_COMPILATION_TV = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ELEVATE_ONLINE = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ELEVATE_RESUMABLE = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET GLOBAL_TEMPORARY_TABLE_AUTO_DROP = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET INTERLEAVED_EXECUTION_TVF = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ISOLATE_SECURITY_POLICY_CARDINALITY = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LIGHTWEIGHT_QUERY_PROFILING = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET OPTIMIZE_FOR_AD_HOC_WORKLOADS = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ROW_MODE_MEMORY_GRANT_FEEDBACK = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET XTP_PROCEDURE_EXECUTION_STATISTICS = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET XTP_QUERY_EXECUTION_STATISTICS = OFF;
GO
USE [CarDealerProject]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_diagramobjects]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
	
GO
/****** Object:  Table [dbo].[Car]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Car](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Vin] [nvarchar](17) NOT NULL,
	[Make] [nvarchar](50) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[Year] [int] NOT NULL,
	[Style] [nvarchar](50) NOT NULL,
	[Transmission] [nvarchar](50) NOT NULL,
	[Drivetrain] [nvarchar](50) NOT NULL,
	[Color] [nvarchar](50) NOT NULL,
	[Odometer] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[New] [nvarchar](50) NOT NULL,
	[Saleable] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[Province] [nvarchar](50) NOT NULL,
	[PostalCode] [nvarchar](50) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[DriversLicense] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employees]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OptionRef]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionRef](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OptionName] [nvarchar](50) NOT NULL,
	[Price] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 2018-11-13 8:40:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[CarId] [int] NOT NULL,
	[OrderDate] [date] NOT NULL,
	[PickupDate] [date] NOT NULL,
	[Warranty] [nvarchar](50) NOT NULL,
	[WinterTires] [nvarchar](50) NOT NULL,
	[RustProofing] [nvarchar](50) NOT NULL,
	[Total] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Car] ON 

INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (5, N'2425EWFGFTEW', N'VW', N'Jetta', 2010, N'Sedan', N'Manual', N'All_Wheel', N'Black', 150000, 6000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (7, N'2425E33RFTEWEE', N'Honda', N'Civic', 2010, N'Sedan', N'Manual', N'All_Wheel', N'Black', 115000, 6000, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (8, N'2425EWFGFTEWQQ', N'Toyota', N'Camry', 2018, N'Sedan', N'Manual', N'All_Wheel', N'Black', 100, 26000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (11, N'2425EWFGFTEW', N'VW', N'Jetta', 2010, N'Sedan', N'Manual', N'All_Wheel', N'Black', 150000, 6000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (12, N'www554455', N'BMW', N'X5', 2014, N'Sedan', N'Manual', N'All_Wheel', N'Black', 0, 0, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (13, N'ERHGRHTB57434', N'Hyundai', N'Elantra', 2009, N'Sedan', N'Automatic', N'All_Wheel', N'Red', 234433, 3500, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (16, N'GSRHT57FGB23', N'Hyundai', N'Elantra', 2010, N'Sedan', N'Automatic', N'Front_Wheel', N'Red', 115000, 5500, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (17, N'DSRGHERG436HG', N'Honda', N'Civic', 2013, N'Sedan', N'Manual', N'Front_Wheel', N'Silver', 105000, 6000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (18, N'dddfaaaaa', N'BMW', N'X3', 2011, N'Sedan', N'Automatic', N'All_Wheel', N'Red', 100000, 8000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (19, N'122121wewereere32', N'Toyota', N'Highlander', 2018, N'Sedan', N'Automatic', N'All_Wheel', N'Silver', 0, 45000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (20, N'kji14785236lkjhgf', N'BMW', N'X1', 2009, N'Sedan', N'Manual', N'All_Wheel', N'White', 30000, 30000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (21, N'444dddds4ddder55d', N'Benz', N'C320', 2014, N'Sedan', N'Manual', N'All_Wheel', N'White', 30000, 29000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (22, N'weee88rrw777ed8fd', N'Honda', N'Civic', 2016, N'Sedan', N'Automatic', N'Front_Wheel', N'Blue', 6000, 24000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (23, N'', N'', N'', 0, N'Sedan', N'Manual', N'All_Wheel', N'', 0, 0, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (24, N'eewwweeeiiiiifood', N'Honda', N'Civic', 2012, N'Sedan', N'Automatic', N'Front_Wheel', N'White', 110000, 15000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (25, N'ddee555678443333d', N'Mazda', N'6', 2017, N'Sedan', N'Automatic', N'All_Wheel', N'Blue', 60000, 13000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (26, N'dddfewweerww2d147', N'Benz', N'S600', 2017, N'Sedan', N'Automatic', N'All_Wheel', N'Silver', 7000, 30000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (27, N'dddfewweerww2d147', N'Benz', N'S600', 2006, N'Sedan', N'Automatic', N'All_Wheel', N'Silver', 170000, 3000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (28, N'wweedddd223365898', N'Toyota', N'Camry', 2010, N'Sedan', N'Automatic', N'All_Wheel', N'red', 80000, 5000, N'Used', N'No')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (29, N'78dee8r777d8fd7s8', N'Toyota', N'Camry', 2014, N'Sedan', N'Automatic', N'Front_Wheel', N'White', 60000, 5500, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (30, N'dd554778wer88987e', N'Honda', N'CRV', 2012, N'Sedan', N'Manual', N'All_Wheel', N'Red', 40000, 18000, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (31, N'RHYERH3453RGHGFRY', N'Honda', N'Civic', 2011, N'Sedan', N'Manual', N'Front_Wheel', N'Silver', 122000, 5500, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (32, N'HGHYTNVHJK6783FGC', N'BMW', N'M3', 2015, N'Sedan', N'Automatic', N'Rear_Wheel', N'Green', 100500, 20000, N'Used', N'Yes')
INSERT [dbo].[Car] ([Id], [Vin], [Make], [Model], [Year], [Style], [Transmission], [Drivetrain], [Color], [Odometer], [Price], [New], [Saleable]) VALUES (33, N'tt112233tt112233t', N'Toyota', N'Camry', 2013, N'Sedan', N'Automatic', N'All_Wheel', N'Black', 120000, 3000, N'Used', N'Yes')
SET IDENTITY_INSERT [dbo].[Car] OFF
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (1, N'JERRY', N'770 STREET', N'MONTREAL', N'QC', N'H1H2X2', N'5142223333', N'JZ111X2222233', N'JERRY@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (2, N'TOM', N'9 STREET', N'LAVAL', N'QC', N'B2B2X6', N'4387773333', N'T3555X2222233', N'TOM@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (3, N'LINDA', N'34 STREET', N'MONTREAL', N'QC', N'H4H4T4', N'5148889999', N'LL444X2222233', N'LINDA@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (4, N'HENRY', N'780 STREET', N'TORONTO', N'ON', N'T5H2X9', N'5142226789', N'HH121X2222233', N'JERRY@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (5, N'AMY', N'3 STREET', N'TORONTO', N'ON', N'H2H6T6', N'4384561111', N'A8444X2222255', N'AMY@YAHOO.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (6, N'JERRY', N'100 STREET', N'MONTREAL', N'QC', N'H1H2X7', N'5142223333', N'J6111X2222233', N'JERRY@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (7, N'MARY', N'2200 STREET', N'LAVAL', N'QC', N'H2B1C1', N'4386663333', N'M772511222233', N'MARY@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (8, N'JERRY', N'999 STREET', N'MONTREAL', N'QC', N'H1H2X2', N'5147772323', N'ZZ111X2222233', N'JERRY@YAHOO.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (9, N'JERRY', N'70 STREET', N'LAVAL', N'QC', N'H1H2X2', N'4382665533', N'ZZ11198798233', N'JERRY@MAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (10, N'TOM', N'2343 SEER ST.', N'DORVAL', N'QC', N'H9H-5H5', N'5148959665', N'GHTGNBGH67834', N'TOM@GMAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (11, N'BILL BURNS', N'546 PINES', N'MONTREAL', N'QC', N'H8T 6F9', N'514-555-6985', N'', N'')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (12, N'MARY MARTHA', N'35 PINEBEACH LANE', N'DORVAL', N'QC', N'H9R-3M7', N'5145145144', N'HRHRT456346HG', N'MM@GMAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (13, N'JOSE DESILVA', N'6523 MIST ST.', N'LAVAL', N'QC', N'H9R-3M7', N'5145142510', N'GHTGBNH567THG', N'JOSE@GMAIL.COM')
INSERT [dbo].[Customers] ([Id], [Name], [Address], [City], [Province], [PostalCode], [PhoneNumber], [DriversLicense], [Email]) VALUES (14, N'ALLANA JAMES', N'562 SILVER ROAD', N'LAVAL', N'QC', N'H9C 7M7', N'5146656636', N'GJUYM67834GT6', N'AJAMES@GMAIL.COM')
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (2, N'John Smith', N'Sales Rep', N'GyppjCfNxGtKsVC08BnyA8RDjMM=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (3, N'Jack Johnson', N'Sales Rep', N'ZqdJ6PLZ7eBHL2YMAwkFOT3dAZY=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (4, N'Terry Coleman', N'Manager', N'NgJbxc+MyqCBnuyTyxefglSxHN4=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (19, N'Tim Bush', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (20, N'Sherry', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (21, N'Jim John', N'Sales Rep', N'2pQuh3FoymNTwqpMcjmiqepQ12U=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (23, N' Linda', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (24, N' Wendy', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (25, N' Linda', N'Sales Rep', N'2jmj7l5rSw0yVb/vlWAYkK/YBwk=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (26, N' Linda', N'Sales Rep', N'2jmj7l5rSw0yVb/vlWAYkK/YBwk=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (27, N'Eric', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (28, N'Bob', N'Sales Rep', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
INSERT [dbo].[Employees] ([Id], [Name], [Title], [Password]) VALUES (29, N'Helen', N'Manager', N'tteV+9WMx1ktlVohk3QzmjI4Aak=')
SET IDENTITY_INSERT [dbo].[Employees] OFF
SET IDENTITY_INSERT [dbo].[OptionRef] ON 

INSERT [dbo].[OptionRef] ([Id], [OptionName], [Price]) VALUES (1, N'Warranties', 500)
INSERT [dbo].[OptionRef] ([Id], [OptionName], [Price]) VALUES (2, N'Winter Tires', 600)
INSERT [dbo].[OptionRef] ([Id], [OptionName], [Price]) VALUES (3, N'Rust Proofing', 200)
SET IDENTITY_INSERT [dbo].[OptionRef] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (2, 1, 1, 3, CAST(N'2018-11-09' AS Date), CAST(N'2018-11-11' AS Date), N'1', N'0', N'0', 6000)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (5, 4, 1, 8, CAST(N'2018-11-06' AS Date), CAST(N'2018-11-30' AS Date), N'0', N'1', N'1', 26800)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (6, 7, 4, 14, CAST(N'2018-11-09' AS Date), CAST(N'2018-11-27' AS Date), N'0', N'1', N'0', 4100)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (7, 8, 1, 6, CAST(N'2017-08-09' AS Date), CAST(N'2017-09-01' AS Date), N'0', N'1', N'1', 1800)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (8, 4, 4, 15, CAST(N'2017-11-09' AS Date), CAST(N'2017-12-06' AS Date), N'0', N'0', N'0', 3500)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (9, 11, 4, 10, CAST(N'2018-09-11' AS Date), CAST(N'2018-09-28' AS Date), N'0', N'0', N'0', 6000)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (10, 4, 1, 9, CAST(N'2018-10-01' AS Date), CAST(N'2018-11-04' AS Date), N'1', N'1', N'1', 4300)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (11, 7, 2, 17, CAST(N'2018-09-05' AS Date), CAST(N'2018-09-27' AS Date), N'0', N'0', N'0', 6000)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (12, 1, 2, 13, CAST(N'2018-11-14' AS Date), CAST(N'2018-11-28' AS Date), N'0', N'0', N'0', 3500)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (13, 1, 2, 13, CAST(N'2018-11-14' AS Date), CAST(N'2018-11-28' AS Date), N'0', N'0', N'0', 3500)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (14, 7, 1, 8, CAST(N'2018-09-03' AS Date), CAST(N'2018-11-21' AS Date), N'0', N'1', N'0', 13600)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (15, 4, 1, 11, CAST(N'2018-11-05' AS Date), CAST(N'2018-11-21' AS Date), N'0', N'0', N'0', 6000)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (16, 1, 1, 18, CAST(N'2018-11-01' AS Date), CAST(N'2018-11-02' AS Date), N'0', N'0', N'0', 8000)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (17, 5, 1, 12, CAST(N'2018-11-25' AS Date), CAST(N'2018-11-30' AS Date), N'0', N'0', N'0', 0)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (18, 4, 1, 5, CAST(N'2018-11-12' AS Date), CAST(N'2018-12-05' AS Date), N'1', N'1', N'0', 4100)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (19, 2, 1, 25, CAST(N'2018-08-21' AS Date), CAST(N'2018-11-21' AS Date), N'0', N'0', N'1', 8200)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (20, 9, 1, 25, CAST(N'2018-09-16' AS Date), CAST(N'2018-10-01' AS Date), N'1', N'1', N'1', 8800)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (21, 1, 1, 22, CAST(N'2018-11-19' AS Date), CAST(N'2018-11-22' AS Date), N'1', N'1', N'1', 7300)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (22, 6, 3, 28, CAST(N'2018-11-12' AS Date), CAST(N'2018-11-14' AS Date), N'0', N'1', N'1', 5800)
INSERT [dbo].[Orders] ([Id], [CustomerId], [EmployeeId], [CarId], [OrderDate], [PickupDate], [Warranty], [WinterTires], [RustProofing], [Total]) VALUES (23, 3, 3, 19, CAST(N'2018-11-14' AS Date), CAST(N'2018-11-17' AS Date), N'0', N'1', N'1', 40300)
SET IDENTITY_INSERT [dbo].[Orders] OFF
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [CK_Car_Drivetrain] CHECK  (([Drivetrain]='Rear_Wheel' OR [Drivetrain]='Front_Wheel' OR [Drivetrain]='All_Wheel'))
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [CK_Car_Drivetrain]
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [CK_Car_New] CHECK  (([New]='Used' OR [New]='New'))
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [CK_Car_New]
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [CK_Car_Saleable] CHECK  (([Saleable]='No' OR [Saleable]='Yes'))
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [CK_Car_Saleable]
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [CK_Car_Style] CHECK  (([Style]='Pickup' OR [Style]='Minivan' OR [Style]='Suv' OR [Style]='Sedan'))
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [CK_Car_Style]
GO
ALTER TABLE [dbo].[Car]  WITH CHECK ADD  CONSTRAINT [CK_Car_Transmission] CHECK  (([Transmission]='Tiptronic' OR [Transmission]='Automatic' OR [Transmission]='Manual'))
GO
ALTER TABLE [dbo].[Car] CHECK CONSTRAINT [CK_Car_Transmission]
GO
/****** Object:  StoredProcedure [dbo].[sp_alterdiagram]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_alterdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null,
		@version 	int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId 			int
		declare @retval 		int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @ShouldChangeUID	int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid ARG', 16, 1)
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();	 
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		revert;
	
		select @ShouldChangeUID = 0
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		
		if(@DiagId IS NULL or (@IsDbo = 0 and @theId <> @UIDFound))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end
	
		if(@IsDbo <> 0)
		begin
			if(@UIDFound is null or USER_NAME(@UIDFound) is null) -- invalid principal_id
			begin
				select @ShouldChangeUID = 1 ;
			end
		end

		-- update dds data			
		update dbo.sysdiagrams set definition = @definition where diagram_id = @DiagId ;

		-- change owner
		if(@ShouldChangeUID = 1)
			update dbo.sysdiagrams set principal_id = @theId where diagram_id = @DiagId ;

		-- update dds version
		if(@version is not null)
			update dbo.sysdiagrams set version = @version where diagram_id = @DiagId ;

		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_creatediagram]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_creatediagram]
	(
		@diagramname 	sysname,
		@owner_id		int	= null, 	
		@version 		int,
		@definition 	varbinary(max)
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
	
		declare @theId int
		declare @retval int
		declare @IsDbo	int
		declare @userName sysname
		if(@version is null or @diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID(); 
		select @IsDbo = IS_MEMBER(N'db_owner');
		revert; 
		
		if @owner_id is null
		begin
			select @owner_id = @theId;
		end
		else
		begin
			if @theId <> @owner_id
			begin
				if @IsDbo = 0
				begin
					RAISERROR (N'E_INVALIDARG', 16, 1);
					return -1
				end
				select @theId = @owner_id
			end
		end
		-- next 2 line only for test, will be removed after define name unique
		if EXISTS(select diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @diagramname)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end
	
		insert into dbo.sysdiagrams(name, principal_id , version, definition)
				VALUES(@diagramname, @theId, @version, @definition) ;
		
		select @retval = @@IDENTITY 
		return @retval
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_dropdiagram]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_dropdiagram]
	(
		@diagramname 	sysname,
		@owner_id	int	= null
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
	
		if(@diagramname is null)
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT; 
		
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		delete from dbo.sysdiagrams where diagram_id = @DiagId;
	
		return 0;
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_helpdiagramdefinition]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_helpdiagramdefinition]
	(
		@diagramname 	sysname,
		@owner_id	int	= null 		
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		set nocount on

		declare @theId 		int
		declare @IsDbo 		int
		declare @DiagId		int
		declare @UIDFound	int
	
		if(@diagramname is null)
		begin
			RAISERROR (N'E_INVALIDARG', 16, 1);
			return -1
		end
	
		execute as caller;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner');
		if(@owner_id is null)
			select @owner_id = @theId;
		revert; 
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname;
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId ))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1);
			return -3
		end

		select version, definition FROM dbo.sysdiagrams where diagram_id = @DiagId ; 
		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_helpdiagrams]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_helpdiagrams]
	(
		@diagramname sysname = NULL,
		@owner_id int = NULL
	)
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		DECLARE @user sysname
		DECLARE @dboLogin bit
		EXECUTE AS CALLER;
			SET @user = USER_NAME();
			SET @dboLogin = CONVERT(bit,IS_MEMBER('db_owner'));
		REVERT;
		SELECT
			[Database] = DB_NAME(),
			[Name] = name,
			[ID] = diagram_id,
			[Owner] = USER_NAME(principal_id),
			[OwnerID] = principal_id
		FROM
			sysdiagrams
		WHERE
			(@dboLogin = 1 OR USER_NAME(principal_id) = @user) AND
			(@diagramname IS NULL OR name = @diagramname) AND
			(@owner_id IS NULL OR principal_id = @owner_id)
		ORDER BY
			4, 5, 1
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_renamediagram]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_renamediagram]
	(
		@diagramname 		sysname,
		@owner_id		int	= null,
		@new_diagramname	sysname
	
	)
	WITH EXECUTE AS 'dbo'
	AS
	BEGIN
		set nocount on
		declare @theId 			int
		declare @IsDbo 			int
		
		declare @UIDFound 		int
		declare @DiagId			int
		declare @DiagIdTarg		int
		declare @u_name			sysname
		if((@diagramname is null) or (@new_diagramname is null))
		begin
			RAISERROR ('Invalid value', 16, 1);
			return -1
		end
	
		EXECUTE AS CALLER;
		select @theId = DATABASE_PRINCIPAL_ID();
		select @IsDbo = IS_MEMBER(N'db_owner'); 
		if(@owner_id is null)
			select @owner_id = @theId;
		REVERT;
	
		select @u_name = USER_NAME(@owner_id)
	
		select @DiagId = diagram_id, @UIDFound = principal_id from dbo.sysdiagrams where principal_id = @owner_id and name = @diagramname 
		if(@DiagId IS NULL or (@IsDbo = 0 and @UIDFound <> @theId))
		begin
			RAISERROR ('Diagram does not exist or you do not have permission.', 16, 1)
			return -3
		end
	
		-- if((@u_name is not null) and (@new_diagramname = @diagramname))	-- nothing will change
		--	return 0;
	
		if(@u_name is null)
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @theId and name = @new_diagramname
		else
			select @DiagIdTarg = diagram_id from dbo.sysdiagrams where principal_id = @owner_id and name = @new_diagramname
	
		if((@DiagIdTarg is not null) and  @DiagId <> @DiagIdTarg)
		begin
			RAISERROR ('The name is already used.', 16, 1);
			return -2
		end		
	
		if(@u_name is null)
			update dbo.sysdiagrams set [name] = @new_diagramname, principal_id = @theId where diagram_id = @DiagId
		else
			update dbo.sysdiagrams set [name] = @new_diagramname where diagram_id = @DiagId
		return 0
	END
	
GO
/****** Object:  StoredProcedure [dbo].[sp_upgraddiagrams]    Script Date: 2018-11-13 8:40:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE PROCEDURE [dbo].[sp_upgraddiagrams]
	AS
	BEGIN
		IF OBJECT_ID(N'dbo.sysdiagrams') IS NOT NULL
			return 0;
	
		CREATE TABLE dbo.sysdiagrams
		(
			name sysname NOT NULL,
			principal_id int NOT NULL,	-- we may change it to varbinary(85)
			diagram_id int PRIMARY KEY IDENTITY,
			version int,
	
			definition varbinary(max)
			CONSTRAINT UK_principal_name UNIQUE
			(
				principal_id,
				name
			)
		);


		/* Add this if we need to have some form of extended properties for diagrams */
		/*
		IF OBJECT_ID(N'dbo.sysdiagram_properties') IS NULL
		BEGIN
			CREATE TABLE dbo.sysdiagram_properties
			(
				diagram_id int,
				name sysname,
				value varbinary(max) NOT NULL
			)
		END
		*/

		IF OBJECT_ID(N'dbo.dtproperties') IS NOT NULL
		begin
			insert into dbo.sysdiagrams
			(
				[name],
				[principal_id],
				[version],
				[definition]
			)
			select	 
				convert(sysname, dgnm.[uvalue]),
				DATABASE_PRINCIPAL_ID(N'dbo'),			-- will change to the sid of sa
				0,							-- zero for old format, dgdef.[version],
				dgdef.[lvalue]
			from dbo.[dtproperties] dgnm
				inner join dbo.[dtproperties] dggd on dggd.[property] = 'DtgSchemaGUID' and dggd.[objectid] = dgnm.[objectid]	
				inner join dbo.[dtproperties] dgdef on dgdef.[property] = 'DtgSchemaDATA' and dgdef.[objectid] = dgnm.[objectid]
				
			where dgnm.[property] = 'DtgSchemaNAME' and dggd.[uvalue] like N'_EA3E6268-D998-11CE-9454-00AA00A3F36E_' 
			return 2;
		end
		return 1;
	END
	
GO
USE [master]
GO
ALTER DATABASE [CarDealerProject] SET  READ_WRITE 
GO
